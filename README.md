# TASK MANAGER

Console Application for task list.

# DEVELOPER INFO

NAME: Sla La

E-MAIL: slala@slala.ru

# SOFTWARE

* JDK 1.8

* MS Windows

# HARDWARE

* RAM 10 Gb

* CPU i5

* HDD 120 Gb

# RUN PROGRAM

```
java -jar ./task-manager.jar
```

# SCREENSHOTS

https://disk.yandex.ru/i/S7g7UbvuS-OFsA


https://disk.yandex.ru/i/TUXDuN6F9FR1xw


https://disk.yandex.ru/i/DVmDNFZ2fbI4Xg

